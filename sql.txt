TUGAS SQL

1 Membuat database

c:\users> cd ..
c:\>cd /xampp
c:\xampp>cd mysql/bin
c:\xampp\mysql\bin>mysql -uroot
MariaDB [(none)]> create database myshop;


2 Membuat table

. MariaDB[(none)]use myshop;
. MariaDB[(myshop)]>CREATE TABLE users (
->id integer auto_increment,
->name varchar(255),
->email varchar(255),
->password varchar(255),
->primary key(id)
->);


.MariaDB[myshop]> CREATE TABLE categories(
->id integer auto_increment,
->name varchar(255),
->primary key(id)
->);

.MariaDB[myshop]>CREATE TABLE items(
->id integer auto_increment,
->name varchar(255),
->description varchar(255),
->price integer,
->stock integer,
->category_id integer,
->primary key(id),
->foreign key(category_id) references categories(id)
->);

 3 Memasukan data pada tabel
#tabel users#
INSERT INTO users value ('','Jhon Doe','Jhon@Doe.com','john123');
INSERT INTO users value ('','Jane Doe','Jane@Doe.com','jenita123');

#tabel categories#
INSERT INTO categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");

#tabel items
INSERT INTO items(name,description,price,stock,category_id) values
("Sumsang b50","hape keren dari merk sumsang","4000000","100","1"),
("Uniklooh","baju keren dari brand ternama","500000","50","2"),
("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");

 4 Ambil data dari database

a ) Mengambil data user
select id,name,email from users;

b ) Mengambil data items
select * from items where price>1000000;
select * from items where name like 'unik%';

c ) Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
 

 5 Mengubah data dari database
select items set price ='2500000' where id ='1';


